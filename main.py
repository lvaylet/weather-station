import board
import digitalio
import busio
import time
import adafruit_bme280
from flask import Flask

# Create library object using I2C bus
i2c = busio.I2C(board.SCL, board.SDA)
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c)

# Grab the sea level pressure at your location from weatheronline.co.uk
# For example, go to Current Weather > Europe > France > Paris Montsouris > Pressure > SL pressure (hPa) for SL pressure in Saint-Cloud
bme280.sea_level_pressure = 1021.0

app = Flask(__name__)

@app.route('/')
def display_sensor_values():
    return 'Temp = {:0.1f} deg C, Humidity = {:0.1f} %, Pressure = {:0.1f} hPa, Altitude = {:0.2f} m'.format(
        bme280.temperature, 
        bme280.humidity, 
        bme280.pressure, 
        bme280.altitude
    )

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
